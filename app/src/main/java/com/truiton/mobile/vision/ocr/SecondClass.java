package com.truiton.mobile.vision.ocr;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class SecondClass extends AppCompatActivity {
    String result;
    TextView text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        result = intent.getStringExtra("text");
        setContentView(R.layout.secondactivity);
        text = findViewById(R.id.text);

        text.setText(result);

    }
}
